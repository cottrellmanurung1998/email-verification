﻿using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using System.Collections.Generic;

namespace emailverification.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        [HttpPost("DailyNotes")]
        public IActionResult SendDailyNotes([FromBody] DailyNotesModel model)
        {
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse("dudley.goyette@ethereal.email"));
            email.To.Add(MailboxAddress.Parse(model.EmailTo));

            email.Subject = "Daily Notes";

            var body = new TextPart(MimeKit.Text.TextFormat.Plain)
            {
                Text = "Your daily notes:\n\n"
            };

            foreach (var note in model.Notes)
            {
                body.Text += $"- {note}\n";
            }

            email.Body = body;

            using var smtp = new SmtpClient();
            smtp.Connect("smtp.ethereal.email", 587, MailKit.Security.SecureSocketOptions.StartTls);
            smtp.Authenticate("dudley.goyette@ethereal.email", "PvFxmr9TkTCVTAmXxQ");
            smtp.Send(email);
            smtp.Disconnect(true);

            return Ok(new { Message = "Email sent successfully." });
        }
    }

    public class DailyNotesModel
    {
        public string EmailTo { get; set; }
        public List<string> Notes { get; set; }
    }
}
